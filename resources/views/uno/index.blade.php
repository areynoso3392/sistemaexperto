@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Huánuco') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class='grid-container_app_c'>
                        <div><a href="uno"><img src="img/uno.png"></a></div>
                    </div>
                    <br>
                    <div class='grid-container_app_c'>
                        <div><img src="img/level.png"></div>
                    </div>
                    <div class="grid-container_app">
                       <ul> <strong>Cuando Se descubrió las manos cruzadas</strong>
                           <li>respuesta a</li>
                           <li>respuesta b</li>
                           <li>respuesta c</li>
                           <li>respuesta d</li>
                           <li>respuesta e</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
