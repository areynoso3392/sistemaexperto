@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Huánuco') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class='grid-container_app_c'>
                        <div><img src="img/level.png"></div>
                    </div>
                    <br><br>
                    <div class='grid-container_app_c'>
                        <div><a href="uno"><img src="img/uno.png"></a></div>
                    </div>
                    <div class="grid-container_app">
                       <div><a href="dos"><img src="img/dos.png"></a></div>
                       <div><a href="tres"><img src="img/tres.png"></a></div>
                    </div>
                    <div class='grid-container_app_c'>
                        <div><a href="cuatro"><img src="img/cuatro.png"></a></div>
                    </div>
                    <br><br>
                    <div class='grid-container_app_c'>
                        <div><img src="img/level2.png"></div>
                    </div>
                    <br><br>
                    <div class='grid-container_app_c'>
                        <div><a href="cinco"><img src="img/cinco.png"></b></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
